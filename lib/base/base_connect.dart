import 'package:get/get.dart';

import 'env_config.dart';

class BaseConnect extends GetConnect {


  @override
  void onInit() {
    httpClient.timeout = const Duration(minutes: 1);
    httpClient.baseUrl = EnvConfig.baseUrl;
  }
}