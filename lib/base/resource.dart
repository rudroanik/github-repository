class Resource<T> {
  ResourceStatus status;
  T? model;
  String? message;
  int code;

  Resource(
      {this.model,
        this.message,
        this.status = ResourceStatus.EMPTY,
        this.code = 0});

  @override
  String toString() {
    return 'Resource{status: $status, model: $model, message: $message, code: $code}';
  }
}

enum ResourceStatus { EMPTY, LOADING, SUCCESS, FAILED }
