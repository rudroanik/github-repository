import 'package:intl/intl.dart';

class DateHelper{
  static String convertDateFormat(String date,
      {String format = "yyyy-MM-dd'T'HH:mm:ssZ"}) {
    DateFormat dateFormat = DateFormat(format);

    DateTime dateTime = dateFormat.parse(date);
    return DateFormat("MM-dd-yy HH:mm").format(dateTime);
  }
}