import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:github_repository/base/app_route.dart';
import 'package:github_repository/landing_page/landing_page_screen.dart';
import 'package:github_repository/repository_details/details_page.dart';
import 'package:github_repository/repository_details/details_page_binding.dart';

import 'db/db_helper.dart';
import 'landing_page/landing_page_binding.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await DBHelper().database;
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Github Repository',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: AppRoutes.homePage,
      getPages: [
        GetPage(name:AppRoutes.homePage, page:()=> LandingPageScreen(),binding: LandingPageBinding()),
        GetPage(name:AppRoutes.detailsPage, page:()=> DetailsPage(),binding: DetailsPageBinding()),
      ],
    );
  }
}
