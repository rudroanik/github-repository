import 'package:get/get.dart';
import 'package:github_repository/repository_details/details_page_controller.dart';
import 'package:github_repository/repository_details/details_page_repository.dart';

import 'details_api_provider.dart';

class DetailsPageBinding extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut(() => DetailsPageApiProvider());
    Get.lazyPut(() => DetailsPageRepository());
    Get.lazyPut(() => DetailsPageController());
  }

}