import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:github_repository/repository_details/details_model.dart';
import 'package:github_repository/repository_details/details_page_controller.dart';
import 'package:github_repository/utils/date_utils.dart';
import 'package:intl/intl.dart';

class DetailsPage extends StatelessWidget {
  const DetailsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Details",
        ),
      ),
      body: GetX<DetailsPageController>(builder: (controller) {
        return controller.isLoading.isTrue
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : DetialsViewWidget(controller.detailsInfo.value);
      }),
    );
  }

  Widget DetialsViewWidget(DetailsModel detailsModel) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: SizedBox(
            height: 100,
            width: 100,
            child: FadeInImage.assetNetwork(
              fit: BoxFit.cover,
              placeholder: "assets/images/loading_new.gif",
              image: detailsModel.avatar_url ?? "",
              placeholderFit: BoxFit.scaleDown,
              placeholderScale: 3,
              imageErrorBuilder: (context, error, stackTrace) {
                return const Icon(Icons.image_not_supported);
              },
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            detailsModel.name ?? "",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(detailsModel.bio ?? "No Bio"),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(detailsModel.updated_at != null
              ? "Updated Time - ${DateHelper.convertDateFormat(detailsModel.updated_at ?? "")}"
              : "No updated date"),
        )
      ],
    );
  }
}
