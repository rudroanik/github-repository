import 'package:get/get.dart';
import 'package:github_repository/repository_details/details_model.dart';
import 'package:github_repository/repository_details/details_page_repository.dart';
import '../base/env_config.dart';

class DetailsPageController extends GetxController {
  Rx<DetailsModel> detailsInfo = Rx<DetailsModel>(DetailsModel());
  RxBool isLoading = true.obs;
  RxString errorMessage = "".obs;

  @override
  void onInit() {
    if (Get.arguments != null) {
      String url = Get.arguments["url"] as String;
      int id = Get.arguments["id"] as int;
      fetchDetailsInfo(removeBaseUrl(url), id);
    }
  }

  Future<void> fetchDetailsInfo(String endPoint, int id) async {
    isLoading.value = true;
    final value =
        await Get.find<DetailsPageRepository>().fetchDetailsInfo(endPoint, id);

    detailsInfo.value = value.model!;

    isLoading.value = false;
  }

  String removeBaseUrl(String url) {
    List<String> split = url.split(EnvConfig.baseUrl);
    return split[1];
  }
}
