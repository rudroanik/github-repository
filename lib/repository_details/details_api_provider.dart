import 'package:github_repository/base/base_connect.dart';
import 'package:github_repository/base/resource.dart';
import 'package:github_repository/repository_details/details_model.dart';

class DetailsPageApiProvider extends BaseConnect {
  Future<Resource<DetailsModel>> fetchDetailsInfo(String endPoint) async {
    final response = await get(endPoint);
    if (response.statusCode == 200) {
      return Resource(
          model: DetailsModel.fromJson(response.body),
          message: "Success",
          status: ResourceStatus.SUCCESS,
          code: 200);
    } else {
      return Resource(
          model: null,
          message: "Faild",
          status: ResourceStatus.FAILED,
          code: 400);
    }
  }
}
