import 'package:get/get.dart';
import 'package:github_repository/base/resource.dart';
import 'package:github_repository/db/db_helper.dart';
import 'package:github_repository/repository_details/details_api_provider.dart';
import 'package:github_repository/repository_details/details_model.dart';

class DetailsPageRepository {
  Future<Resource<DetailsModel>> fetchDetailsInfo(
      String endPoint, int id) async {
    List<DetailsModel> details = [];

    final value =
        await Get.find<DetailsPageApiProvider>().fetchDetailsInfo(endPoint);

    final List<Map<String, dynamic>> tasks = await DBHelper().queryAllRows(id);
    details.addAll(tasks.map((data) => DetailsModel.fromJson(data)).toList());
    if (details.isNotEmpty) {
      details.forEach((element) async {
        if (element.id != id) {
          await DBHelper().insertDetailsInfo(value.model ?? DetailsModel());
        }
      });
    } else {
      await DBHelper().insertDetailsInfo(value.model ?? DetailsModel());
    }
    return details.isNotEmpty
        ? Resource(model: details[0], status: ResourceStatus.SUCCESS, code: 200)
        : value;
  }
}
