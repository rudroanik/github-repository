import 'package:json_annotation/json_annotation.dart';
part 'details_model.g.dart';

@JsonSerializable()
class DetailsModel{
  int? id;
  String? name;
  String? bio;
  String? avatar_url;
  String? updated_at;

  DetailsModel({this.id, this.name, this.bio, this.avatar_url, this.updated_at});

  factory DetailsModel.fromJson(Map<String, dynamic> json) =>
      _$DetailsModelFromJson(json);

  @override
  String toString() {
    return 'DetailsModel{id: $id, name: $name, bio: $bio, avatar_url: $avatar_url, updated_at: $updated_at}';
  }
}