// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'details_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DetailsModel _$DetailsModelFromJson(Map<String, dynamic> json) => DetailsModel(
      id: json['id'] as int?,
      name: json['name'] as String?,
      bio: json['bio'] as String?,
      avatar_url: json['avatar_url'] as String?,
      updated_at: json['updated_at'] as String?,
    );

Map<String, dynamic> _$DetailsModelToJson(DetailsModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'bio': instance.bio,
      'avatar_url': instance.avatar_url,
      'updated_at': instance.updated_at,
    };
