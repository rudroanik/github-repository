import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceHelper {
  Future<void> cacheSelectedSortedOption(String name) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString("sortedBy", name);
  }

  Future<String> getSelectedSortedOption() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getString("sortedBy") ?? "";
  }
}
