import 'package:github_repository/landing_page/landing_page_model.dart';
import 'package:github_repository/repository_details/details_model.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DBHelper {
  static const _databaseName = 'github_repo.db';
  static const _details_table = 'details_table';
  static const _repository_table = 'repository_table';
  static const _databaseVersion = 1;
  static Database? _database;

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDB();
    return _database!;
  }

  _initDB() async {
    String path = join(await getDatabasesPath(), _databaseName);

    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  _onCreate(Database db, int version) async {
    await db.execute('CREATE TABLE $_details_table('
        'id INTEGER PRIMARY KEY, name TEXT, bio STRING,avatar_url STRING, updated_at STRING'
        ')');
    await db.execute('CREATE TABLE $_repository_table('
        'repo_id INTEGER PRIMARY KEY AUTOINCREMENT,id INTEGER,name TEXT, full_name STRING,description STRING, language STRING,url STRING,updated_at STRING,stargazers_count INTEGER'
        ')');
  }

  Future<int> insertRepository(Item item) async {
    Database? db = await DBHelper._database;
    return await db!.insert(_repository_table, {
      'id': item.owner?.id,
      'name': item.name,
      'full_name': item.full_name,
      'description': item.description,
      'language': item.language,
      'url': item.owner?.url,
      'updated_at': item.updated_at,
      'stargazers_count': item.stargazers_count,
    });
  }

  Future<int> insertDetailsInfo(DetailsModel detailsInfo) async {
    Database? db = await DBHelper._database;
    return await db!.insert(_details_table, {
      'id': detailsInfo.id,
      'name': detailsInfo.name,
      'bio': detailsInfo.bio,
      'avatar_url': detailsInfo.avatar_url,
      'updated_at': detailsInfo.updated_at,
    });
  }

  Future<List<Map<String, dynamic>>> queryAllRows(int id) async {
    Database? db = await DBHelper._database;
    return await db!.rawQuery('SELECT * FROM $_details_table WHERE id=?', [id]);

    // return await db!.query(_details_table);
  }

  Future<List<Map<String, dynamic>>> getAllRepositories(String sortBy) async {
    Database? db = await DBHelper._database;
    return await db!.query(_repository_table, orderBy: "$sortBy DESC");
  }
}
