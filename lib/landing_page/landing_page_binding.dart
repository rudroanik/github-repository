import 'package:get/get.dart';
import 'package:github_repository/landing_page/landing_page_api_provider.dart';
import 'package:github_repository/landing_page/landing_page_controller.dart';
import 'package:github_repository/landing_page/landing_page_repository.dart';

class LandingPageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LandingPageApiProvider());
    Get.lazyPut(() => LandingPageRepository());
    Get.lazyPut(() => LandingPageController());
  }
}
