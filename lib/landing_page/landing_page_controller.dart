import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:github_repository/db/db_helper.dart';
import 'package:github_repository/db/sharedPref_helper.dart';
import 'package:github_repository/landing_page/landing_page_repository.dart';

import 'landing_page_model.dart';

class LandingPageController extends GetxController with ScrollMixin {
  RxList<Item> repositories = <Item>[].obs;
  final int perPage = 10;
  int page = 1;
  bool getFirstData = false;
  bool lastPage = false;
  RxBool isLoading = true.obs;
  int statusCode = 0;

  Future<void> fetchRepositories(String sortBy, {bool loading = true}) async {
    isLoading.value = loading;
    Map<String, String> queryParams = {
      "q": "flutter",
      "page": page.toString(),
      "sort": sortBy,
      "per_page": perPage.toString()
    };
    final value = await Get.find<LandingPageRepository>()
        .fetchRepositories(queryParams, sortBy);

    statusCode = value.code;

    if (value.code == 200) {
      if (!getFirstData && value.model!.items.isEmpty) {
      } else if (getFirstData && value.model!.items.isEmpty) {
        lastPage = true;
      } else {
        getFirstData = true;

        repositories.addAll(value.model!.items);
      }
    }
    //201 used for checking it's local db response
    else if (value.code == 201) {
      repositories.clear();
      repositories.addAll(value.model!.items);
    }
    isLoading.value = false;
  }

  @override
  Future<void> onEndScroll() async {
    Get.dialog(const Center(child: CircularProgressIndicator()));
    await fetchRepositories("", loading: false);
    if (!lastPage) {
      if (statusCode == 200) {
        page += 1;
      }
      Get.back();
    }
  }

  @override
  Future<void> onTopScroll() async {
    debugPrint("top scroll");
  }
}
