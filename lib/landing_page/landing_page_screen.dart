import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';
import 'package:github_repository/base/app_route.dart';
import 'package:github_repository/db/sharedPref_helper.dart';
import 'package:github_repository/landing_page/landing_page_controller.dart';
import 'package:github_repository/landing_page/landing_page_model.dart';
import 'package:github_repository/landing_page/landing_page_repository.dart';
import 'package:github_repository/utils/date_utils.dart';

class LandingPageScreen extends StatelessWidget {
  List<String> sortedBy = ["Updated Date", "Star"];
  String? selectedValue;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Github Repositories",
        ),
      ),
      body: Column(
        children: [
          FutureBuilder<String>(
              future: SharedPreferenceHelper().getSelectedSortedOption(),
              builder: (_, data) {
                if (data.hasData) {
                  if (data.data!.isNotEmpty) {
                    selectedValue = data.data;
                  }
                }

                return Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      width: Get.width / 2,
                      child: DropdownButtonFormField(
                        hint: const Text("Select sort option"),
                        icon: const Icon(Icons.keyboard_arrow_down_sharp),
                        items: sortedBy.map((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        value: selectedValue,
                        onChanged: (String? value) {
                          if (value == sortedBy[0]) {
                            SharedPreferenceHelper()
                                .cacheSelectedSortedOption(sortedBy[0]);
                            Get.find<LandingPageController>()
                                .fetchRepositories("updated");
                          } else {
                            SharedPreferenceHelper()
                                .cacheSelectedSortedOption(sortedBy[1]);
                            Get.find<LandingPageController>()
                                .fetchRepositories("stars");
                          }
                        },
                        decoration:
                            const InputDecoration(enabledBorder: InputBorder.none),
                      ),
                    ),
                  ),
                );
              }),
          Expanded(
            child: GetX<LandingPageController>(
                initState: (_) => Get.find<LandingPageController>()
                    .fetchRepositories(
                        selectedValue == sortedBy[0] ? "updated" : "stars"),
                builder: (controller) {
                  return controller.isLoading.isTrue
                      ? const Center(
                          child: CircularProgressIndicator(),
                        )
                      : controller.repositories.isNotEmpty
                          ? ListView.builder(
                              controller: controller.scroll,
                              itemCount: controller.repositories.length,
                              itemBuilder: (_, index) {
                                return ItemWidget(
                                    controller.repositories[index]);
                              })
                          : const Center(
                              child: Text("No Repository Found"),
                            );
                }),
          ),
        ],
      ),
    );
  }

  Widget ItemWidget(Item item) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Card(
        child: InkWell(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ItemRowWidget("${item.name}(${item.full_name})"),
              ItemRowWidget(item.language ?? ""),
              ItemRowWidget(item.description ?? "No Description"),
              ItemRowWidget("Star- ${item.stargazers_count}"),
              ItemRowWidget(
                  "Updated Time - ${DateHelper.convertDateFormat(item.updated_at!)}")
            ],
          ),
          onTap: () {
            Get.toNamed(AppRoutes.detailsPage,
                arguments: {"url": item.owner?.url, "id": item.owner?.id});
          },
        ),
      ),
    );
  }

  Widget ItemRowWidget(String name) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Text(name),
    );
  }
}
