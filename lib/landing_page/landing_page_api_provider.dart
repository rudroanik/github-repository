import 'package:github_repository/base/api_end_point.dart';
import 'package:github_repository/base/base_connect.dart';
import 'package:github_repository/base/resource.dart';

import 'landing_page_model.dart';

class LandingPageApiProvider extends BaseConnect{

  Future<Resource<LandingPageModel>> fetchRepositories(Map<String, String> queryParams)async{
    final response =
        await get(ApiEndPoint.searchRepositories, query: queryParams);
    if (response.statusCode == 200) {
      return Resource(
          model: LandingPageModel.fromJson(response.body),
          message: "Success",
          status: ResourceStatus.SUCCESS,
          code: 200);
    } else {
      return Resource(
          model: null,
          message: "Faild",
          status: ResourceStatus.FAILED,
          code: 400);
    }
  }
}