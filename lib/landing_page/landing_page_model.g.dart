// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'landing_page_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LandingPageModel _$LandingPageModelFromJson(Map<String, dynamic> json) =>
    LandingPageModel(
      (json['items'] as List<dynamic>)
          .map((e) => Item.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$LandingPageModelToJson(LandingPageModel instance) =>
    <String, dynamic>{
      'items': instance.items,
    };

Item _$ItemFromJson(Map<String, dynamic> json) => Item(
      json['name'] as String?,
      json['full_name'] as String?,
      json['description'] as String?,
      json['private'] as bool?,
      json['owner'] == null
          ? null
          : Owner.fromJson(json['owner'] as Map<String, dynamic>),
      json['language'] as String?,
      json['updated_at'] as String?,
      json['stargazers_count'] as int,
    );

Map<String, dynamic> _$ItemToJson(Item instance) => <String, dynamic>{
      'name': instance.name,
      'full_name': instance.full_name,
      'description': instance.description,
      'private': instance.private,
      'owner': instance.owner,
      'language': instance.language,
      'updated_at': instance.updated_at,
      'stargazers_count': instance.stargazers_count,
    };

Owner _$OwnerFromJson(Map<String, dynamic> json) => Owner(
      json['id'] as int,
      json['url'] as String?,
    );

Map<String, dynamic> _$OwnerToJson(Owner instance) => <String, dynamic>{
      'id': instance.id,
      'url': instance.url,
    };
