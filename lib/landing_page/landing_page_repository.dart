import 'package:get/get.dart';
import 'package:github_repository/base/resource.dart';
import 'package:github_repository/db/db_helper.dart';
import 'package:github_repository/landing_page/landing_page_api_provider.dart';

import 'landing_page_model.dart';

class LandingPageRepository {
  Future<Resource<LandingPageModel>> fetchRepositories(
      Map<String, String> queryParams, String sortBy) async {
    List<Item> items = [];

    final value =
        await Get.find<LandingPageApiProvider>().fetchRepositories(queryParams);

    if (value.code == 200) {
      if (value.model != null && value.model!.items.isNotEmpty) {
        value.model?.items.forEach((element) async {
          await DBHelper().insertRepository(element);
        });
      }
    } else {
      final List<Map<String, dynamic>> repositories =
          await DBHelper().getAllRepositories(getSortValue(sortBy));
      items.addAll(repositories.map((e) => Item(
          e["name"],
          e["full_name"],
          e["description"],
          e["private"],
          Owner(e["id"], e["url"]),
          e["language"],
          e["updated_at"],
          e["stargazers_count"])));
    }

    return value.model != null && value.model!.items.isNotEmpty
        ? value
        : Resource(
            model: LandingPageModel(items),
            status: ResourceStatus.SUCCESS,
            code: 201);
  }

  String getSortValue(String sortBy) {
    if (sortBy == "updated") {
      return "updated_at";
    } else if (sortBy == "stars") {
      return "stargazers_count";
    } else {
      return "name";
    }
  }
}
