import 'package:json_annotation/json_annotation.dart';

part 'landing_page_model.g.dart';

@JsonSerializable()
class LandingPageModel {
  List<Item> items;

  LandingPageModel(this.items);

  factory LandingPageModel.fromJson(Map<String, dynamic> json) =>
      _$LandingPageModelFromJson(json);

  static List<LandingPageModel> listFromJson(list) =>
      List<LandingPageModel>.from(list.map((x) => LandingPageModel.fromJson(x)))
          .toList();
}

@JsonSerializable()
class Item {
  String? name;
  String? full_name;
  String? description;
  bool? private;
  Owner? owner;
  String? language;
  String? updated_at;
  int stargazers_count;

  Item(this.name, this.full_name, this.description, this.private, this.owner,
      this.language, this.updated_at, this.stargazers_count);

  factory Item.fromJson(Map<String, dynamic> json) => _$ItemFromJson(json);

  @override
  String toString() {
    return 'Item{name: $name, full_name: $full_name, description: $description, private: $private, owner: $owner, language: $language}';
  }
}

@JsonSerializable()
class Owner {
  int id;
  String? url;

  Owner(this.id, this.url);

  factory Owner.fromJson(Map<String, dynamic> json) => _$OwnerFromJson(json);

  @override
  String toString() {
    return 'Owner{id: $id, url: $url}';
  }
}
