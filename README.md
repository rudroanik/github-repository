# Github Repository App

#Feature
1.User can see list of github flutter repositories offline & online mode.
2.User can see repository details offline & online mode;
3. User can sort list based on updated date & stars.
4. Selected sorting option persists in further app sessions.
5.Pagination implemented. At a time API fetch 10 items.


#Screen shoot
![List Of Repositories](assets/screen_shoot/Screenshot_1660537380.png)
![Sorted Option](assets/screen_shoot/Screenshot_1660537386.png)
![Details Page](assets/screen_shoot/Screenshot_1660537398.png)


